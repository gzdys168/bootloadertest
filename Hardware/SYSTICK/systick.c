#include "myinclude.h"

/******************************************************************************
//变量定义
******************************************************************************/
volatile uint32_t b100ms;
volatile uint32_t systickcnt; //系统节拍数

/******************************************************************************
*功能：初始化系统节拍
*参数：Nothing
*返回：Nothing
******************************************************************************/
void Init_Systick(void)
{
	b100ms = 0;
	systickcnt = 0;

	//Initialize System Tick with 10ms time interval
	SYSTICK_InternalInit(MY_SYSTICK_DELAY);

	//Enable System Tick interrupt
	SYSTICK_IntCmd(ENABLE);

	//Enable System Tick Counter
	SYSTICK_Cmd(ENABLE);
}

/******************************************************************************
 * @brief 		SysTick interrupt handler
 * @param		None
 * @return 		None
 *****************************************************************************/
void SysTick_Handler(void)
{
	SYSTICK_ClearCounterFlag();
	
	systickcnt++;
	
	if ((systickcnt % MY_SYSTICK_100MS_CNT) == 0)
	{
		b100ms = 1;
	}
	
	if (uart0cnt > 0)
	{
		uart0cycle++;
	}
}
