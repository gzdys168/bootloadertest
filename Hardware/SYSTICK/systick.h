#ifndef __SYSTICK_H
#define __SYSTICK_H
#include "myinclude.h"

/******************************************************************************
//常量定义
******************************************************************************/
#define MY_SYSTICK_DELAY 		10 //ms
#define MY_SYSTICK_50MS_CNT 	(50 / MY_SYSTICK_DELAY)
#define MY_SYSTICK_100MS_CNT 	(100 / MY_SYSTICK_DELAY)
#define MY_SYSTICK_200MS_CNT 	(200 / MY_SYSTICK_DELAY)
#define MY_SYSTICK_500MS_CNT 	(500 / MY_SYSTICK_DELAY)
#define MY_SYSTICK_1S_CNT 		(1000 / MY_SYSTICK_DELAY)
#define MY_SYSTICK_10S_CNT 		(10000 / MY_SYSTICK_DELAY)

/******************************************************************************
//变量声明
******************************************************************************/
extern volatile uint32_t b100ms;
extern volatile uint32_t systickcnt;

/******************************************************************************
//函数声明
******************************************************************************/
void Init_Systick(void);

#endif
