#ifndef __UART0_H
#define __UART0_H
#include "myinclude.h"

/******************************************************************************
//常量定义
******************************************************************************/
#define UART0_BUFF_SIZE		    255

/******************************************************************************
//IO口定义
******************************************************************************/
#define UART0_TX_PORT			0
#define UART0_TX_PIN			2 //输出，98

#define UART0_RX_PORT			0
#define UART0_RX_PIN			3 //输入，99

/******************************************************************************
//变量声明
******************************************************************************/
extern volatile uint32_t uart0cnt;
extern volatile uint32_t uart0cycle;
extern uint8_t uart0Buff[UART0_BUFF_SIZE];

/******************************************************************************
//函数声明
******************************************************************************/
void InitUart0Buff(void);
void InitUART0(uint32_t Baud_rate);
void UART0Send(uint8_t *BufferPtr, uint32_t Length);

#endif
