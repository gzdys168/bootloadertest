#include "myinclude.h"

/******************************************************************************
//变量定义
******************************************************************************/
static volatile uint8_t recReturn;
volatile uint32_t uart0cnt;
volatile uint32_t uart0cycle;
uint8_t uart0Buff[UART0_BUFF_SIZE];

#ifdef WITH_AUTO_BAUDRATE_FUNC //自动波特率
    __IO FlagStatus Synchronous = RESET;
#endif

/******************************************************************************
//初始化串口0接收
******************************************************************************/
void InitUart0Buff(void)
{
	uart0cnt = 0;
	uart0cycle = 0;
	recReturn = 0;
}

/******************************************************************************
//初始化串口0通讯
******************************************************************************/
void InitUART0(uint32_t Baud_rate)
{
	PINSEL_CFG_Type PinCfg;
	UART_CFG_Type UARTConfigStruct;
	UART_FIFO_CFG_Type UARTFIFOConfigStruct;
    
#ifdef WITH_AUTO_BAUDRATE_FUNC //自动波特率
    // Auto baudrate configuration structure
    UART_AB_CFG_Type ABConfig;
#endif

	InitUart0Buff(); //初始化串口0接收
	
	//Initialize UART0 pin connect
	//UART0_TX
	PinCfg.Portnum = UART0_TX_PORT;
	PinCfg.Pinnum = UART0_TX_PIN;
	PinCfg.Funcnum = PINSEL_FUNC_1;
	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	PinCfg.Pinmode = PINSEL_PINMODE_NORMAL;
	PINSEL_ConfigPin(&PinCfg);    
    GPIO_SetDir(UART0_TX_PORT, (1<<UART0_TX_PIN), IO_DIR_OUT);
	
	//UART0_RX
	PinCfg.Portnum = UART0_RX_PORT;
	PinCfg.Pinnum = UART0_RX_PIN;
	PINSEL_ConfigPin(&PinCfg);    
    GPIO_SetDir(UART0_RX_PORT, (1<<UART0_RX_PIN), IO_DIR_IN);

	// Initialize UART0 peripheral with given to corresponding parameter
	UARTConfigStruct.Baud_rate = Baud_rate;
	UARTConfigStruct.Databits = UART_DATABIT_8;
	UARTConfigStruct.Parity = UART_PARITY_NONE;
	UARTConfigStruct.Stopbits = UART_STOPBIT_1; //0
	UART_Init((LPC_UART_TypeDef *)LPC_UART0, &UARTConfigStruct);

	// Initialize FIFO for UART0 peripheral
	UARTFIFOConfigStruct.FIFO_ResetRxBuf = ENABLE;
	UARTFIFOConfigStruct.FIFO_ResetTxBuf = ENABLE;
	UARTFIFOConfigStruct.FIFO_DMAMode = DISABLE;
	UARTFIFOConfigStruct.FIFO_Level = UART_FIFO_TRGLEV0;
	UART_FIFOConfig((LPC_UART_TypeDef *)LPC_UART0, &UARTFIFOConfigStruct);

	// Enable UART Transmit
	UART_TxCmd((LPC_UART_TypeDef *)LPC_UART0, ENABLE);
    
#ifdef WITH_AUTO_BAUDRATE_FUNC //自动波特率
    /* Enable UART End of Auto baudrate interrupt */
    UART_IntConfig((LPC_UART_TypeDef *)LPC_UART0, UART_INTCFG_ABEO, ENABLE);
    
    /* Enable UART Auto baudrate timeout interrupt */
    UART_IntConfig((LPC_UART_TypeDef *)LPC_UART0, UART_INTCFG_ABTO, ENABLE);
#endif

    /* Enable UART Rx interrupt */
	UART_IntConfig((LPC_UART_TypeDef *)LPC_UART0, UART_INTCFG_RBR, ENABLE);
	
	/* Enable UART line status interrupt */
	UART_IntConfig((LPC_UART_TypeDef *)LPC_UART0, UART_INTCFG_RLS, ENABLE);

    /* preemption = 1, sub-priority = 1 */
    //NVIC_SetPriority(UART0_IRQn, UART0_PRIORITY_VALUE); //((0x01<<3)|0x01);

	/* Enable Interrupt for UART0 channel */
    NVIC_EnableIRQ(UART0_IRQn);
    
#ifdef WITH_AUTO_BAUDRATE_FUNC //自动波特率
    // Configure Auto baud rate mode
    ABConfig.ABMode = UART_AUTOBAUD_MODE0;
    ABConfig.AutoRestart = ENABLE;

    // Start auto baudrate mode
    UART_ABCmd(LPC_UART0, &ABConfig, ENABLE);
#endif
}

/******************************************************************************
 * @brief 		UART receive function (ring buffer used)
 * @param[in]	None
 * @return 		None
******************************************************************************/
static void UART0_INTReceive(void)
{
	uint8_t tmp;
	
	tmp = UART_ReceiveByte(LPC_UART0);
	
	if (uart0cnt < UART0_BUFF_SIZE)
	{
		uart0Buff[uart0cnt++] = tmp;
        
		if (recReturn == 1)
		{
			if (tmp == '\n')
				ProcessUART0();
            else if (tmp != '\r')
                recReturn = 0;
		}
		else if (tmp == '\r')
		{
			recReturn = 1;
		}
	}
	
	else //接收已满
	{
		InitUart0Buff();
		uart0Buff[uart0cnt++] = tmp;
	}
}

/******************************************************************************
//串口0中断处理函数
******************************************************************************/
void UART0_IRQHandler(void)
{
	uint32_t intsrc, tmp, tmp1;
	
	uart0cycle = 0;

	/* Determine the interrupt source */
	intsrc = UART_GetIntId(LPC_UART0);
	tmp = intsrc & UART_IIR_INTID_MASK;

	// Receive Line Status
	if (tmp == UART_IIR_INTID_RLS)
	{
		// Check line status
		tmp1 = UART_GetLineStatus(LPC_UART0);
		// Mask out the Receive Ready and Transmit Holding empty status
		tmp1 &= (UART_LSR_OE | UART_LSR_PE | UART_LSR_FE | UART_LSR_BI | UART_LSR_RXFE);
		// If any error exist
		if (tmp1) 
		{
			__set_FAULTMASK(1);
			NVIC_SystemReset();
			//UART_IntErr(tmp1); //by jjh
		}
	}

	// Receive Data Available or Character time-out
	if ((tmp == UART_IIR_INTID_RDA) || (tmp == UART_IIR_INTID_CTI))
	{
		UART0_INTReceive();
	}

	// Transmit Holding Empty
	if (tmp == UART_IIR_INTID_THRE)
	{
		//UART_IntTransmit(); //by jjh
	}
    
#ifdef WITH_AUTO_BAUDRATE_FUNC //自动波特率
    intsrc &= (UART_IIR_ABEO_INT | UART_IIR_ABTO_INT);
    // Check if End of auto-baudrate interrupt or Auto baudrate time out
    if (intsrc)
    {
        // Clear interrupt pending
        if (intsrc & UART_IIR_ABEO_INT)
            UART_ABClearIntPending(LPC_UART0, UART_AUTOBAUD_INTSTAT_ABEO);
        
        if (intsrc & UART_IIR_ABTO_INT)
            UART_ABClearIntPending(LPC_UART0, UART_AUTOBAUD_INTSTAT_ABTO);
        
        if (Synchronous == RESET)
        {
            /* Interrupt caused by End of auto-baud */
            if (intsrc & UART_AUTOBAUD_INTSTAT_ABEO)
            {
                // Disable AB interrupt
                UART_IntConfig(LPC_UART0, UART_INTCFG_ABEO, DISABLE);
                // Set Sync flag
                Synchronous = SET;
            }

            /* Auto-Baudrate Time-Out interrupt (not implemented) */
            if (intsrc & UART_AUTOBAUD_INTSTAT_ABTO)
            {
                /* Just clear this bit - Add your code here */
                UART_ABClearIntPending(LPC_UART0, UART_AUTOBAUD_INTSTAT_ABTO);
            }
        }
    }
#endif
}

/******************************************************************************
*功能：发送数据(十六进制)
*参数：*BufferPtr : 发送缓冲区, Length : 缓冲区大小
*返回：Nothing
******************************************************************************/
void UART0Send(uint8_t *BufferPtr, uint32_t Length)
{
	UART_Send(LPC_UART0, BufferPtr, Length, BLOCKING);
}
