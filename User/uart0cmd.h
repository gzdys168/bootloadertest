#ifndef __UART0CMD_H
#define __UART0CMD_H
#include "myinclude.h"

/******************************************************************************
//常量定义
******************************************************************************/
//命令格式：*HHHMLD...DXEE------------------------------------------------------
//*     --- 命令头(1)
//HHH   --- 设备编号(3)
//M     --- 通讯命令(1)
//L     --- 数据长度(1)
//D...D --- 数据(0..n)
//X     --- 数据校验(1)
//EE    --- 命令结束符(2)
#define COM_CMD_HEAD_IDX		0										//0
#define COM_CMD_DEV_IDX 		(COM_CMD_HEAD_IDX + 1)					//1
#define COM_CMD_DEV_SIZE 		3
#define COM_CMD_IDX		 		(COM_CMD_DEV_IDX + COM_CMD_DEV_SIZE)	//4
#define COM_CMD_LEN_IDX			(COM_CMD_IDX + 1)						//5
#define COM_CMD_DATA_IDX		(COM_CMD_LEN_IDX + 1)					//6
//#define COM_CMD_DATA_SIZE		//可能为0
//#define COM_CMD_CHK_IDX		//存在的
#define COM_CMD_END_FLAG1		0x0D
#define COM_CMD_END_FLAG2		0x0A

#define COM_CMD_MIN_SIZE		(COM_CMD_LEN_IDX + 1 + 3) //9

//通讯命令----------------------------------------------------------------------
#define COM_CMD_SOFT_VERSION	0xA5

#define COM_CMD_IAP     		0x99

/******************************************************************************
//函数声明
******************************************************************************/
void ProcessUART0(void);

#endif
