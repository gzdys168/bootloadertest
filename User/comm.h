#ifndef __COMM_H
#define __COMM_H
#include "myinclude.h"

/******************************************************************************
//��������
******************************************************************************/
void DelayUs(uint32_t No);
void DelayMs(uint32_t No);
uint8_t MyStrEQ(const uint8_t *a, const uint8_t *b, uint8_t ilen);
uint8_t MyStrCMP(const uint8_t *a, const uint8_t *b, uint8_t len);
void MyStrCPY(uint8_t *d, const uint8_t *r, uint8_t ilen);
uint8_t GetXOR(const uint8_t *ptr, uint8_t len);
uint8_t GetSum(const uint8_t *b, uint8_t icount);

void PrintCharByRTT(const uint8_t *buff, uint32_t Length);
void PrintHexByRTT(const uint8_t *buff, uint32_t Length);

#endif
