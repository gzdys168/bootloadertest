#include "myinclude.h"

/******************************************************************************
//常量定义
******************************************************************************/
#define vu32 volatile unsigned int

#define IAP_SIZE    		1024
#define USE_SECTOR_COUNT    16 //使用扇区个数

static const uint32_t use_sertor_list[USE_SECTOR_COUNT] = 
{
//SECTOR_0_START, //4K
//SECTOR_1_START, //4K
//SECTOR_2_START, //4K
SECTOR_3_START, //4K，配置地址与应用程序跳转地址依此而定！
SECTOR_4_START, //4K
SECTOR_5_START, //4K
SECTOR_6_START, //4K
SECTOR_7_START, //4K，32K
SECTOR_8_START, //4K
SECTOR_9_START, //4K
SECTOR_10_START, //4K
SECTOR_11_START, //4K
SECTOR_12_START, //4K
SECTOR_13_START, //4K
SECTOR_14_START, //4K
SECTOR_15_START, //4K，64K
SECTOR_16_START, //32K
SECTOR_17_START, //32K，128K
SECTOR_18_START, //32K
//SECTOR_19_START, //32K
//SECTOR_20_START, //32K
//SECTOR_21_START, //32K，256K
//SECTOR_22_START, //32K
//SECTOR_23_START, //32K
//SECTOR_24_START, //32K
//SECTOR_25_START, //32K
//SECTOR_26_START, //32K
//SECTOR_27_START, //32K
//SECTOR_28_START, //32K
//SECTOR_29_START, //32K，512K
};

/******************************************************************************
//变量定义
******************************************************************************/
uint8_t __attribute__ ((aligned (4))) iapbuf[IAP_SIZE];	//用于缓存数据的数组
uint16_t iapcnt = 0;	    							//当前iapbuffer中已经填充的数据长度,一次填充满了之后写入flash并清零
uint32_t iapaddr = SECTOR_3_START;						//当前系统写入地址,每次写入之后地址增加1024

/******************************************************************************
//IAP擦除
******************************************************************************/
uint32_t iap_erase(void)
{
	uint32_t i;
	uint32_t rst;
	uint32_t result[4];
	uint32_t isec;
	IAP_STATUS_CODE status;
	
	iapcnt = 0;
	iapaddr = SECTOR_3_START;
	
	__set_PRIMASK(1); //关闭总中断
	
	rst = 1;
	for (i = 0; i < USE_SECTOR_COUNT; i++)
	{
		isec = GetSecNum(use_sertor_list[i]);
		
		status = EraseSector(isec, isec);
		if(status != CMD_SUCCESS)
		{
			rst = 0;
			break;
		}

		status = BlankCheckSector(isec, isec, &result[0], &result[1]);
		if(status != CMD_SUCCESS)
		{
			rst = 0;
			break;
		}
	}
	
	__set_PRIMASK(0); //开放总中断
	
	return rst;
}

/******************************************************************************
//写Flash
******************************************************************************/
uint32_t write_flash(void)
{
	uint8_t *ptr;
	IAP_STATUS_CODE status;
	
	ptr = (uint8_t*)(iapaddr);
	status =  CopyRAM2Flash(ptr, iapbuf, IAP_WRITE_1024);
	
	if (status == CMD_SUCCESS) //写成功
	{
		status =  Compare(ptr, iapbuf, IAP_WRITE_1024);
		if (status == CMD_SUCCESS) //比较成功
		{
			iapcnt = 0;
			iapaddr += IAP_SIZE;
		}
		else //比较失败
		{
			//SEGGER_RTT_WriteString(0, "Compare() failed\r\n"); //Debug
			return 0;
		}
	}
	else //写失败
	{
		//SEGGER_RTT_WriteString(0, "CopyRAM2Flash() failed\r\n"); //Debug
		return 0;
	}
	
	return 1;
}

/******************************************************************************
//IAP数据
******************************************************************************/
uint32_t iap_data(uint8_t *buf, uint8_t cnt)
{
	CalculateCRC(buf, cnt - 2);
	
	if ((buf[cnt-2] == crchigh) && (buf[cnt-1] == crclow))
	{
		MyStrCPY(iapbuf+iapcnt, buf, cnt-2);
		iapcnt += (cnt - 2); 
		
		if (iapcnt == IAP_SIZE)
		{
			return write_flash();
		}
		
		return 1;
	}
	
	return 0;
}

/******************************************************************************
//结束IAP
******************************************************************************/
uint32_t end_iap(void)
{
	if (iapcnt > 0)
	{
		while (iapcnt < IAP_SIZE)
		{
			iapbuf[iapcnt++] = 0xFF;
		}
		
		if (write_flash() == 0)
			return 0;
	}
	
	iapcnt = 0;
	iapaddr = use_sertor_list[0] - IAP_SIZE;
	while (iapcnt < IAP_SIZE)
	{
		iapbuf[iapcnt++] = 0xFF;
	}	
	iapbuf[IAP_SIZE - 3] = uart0Buff[COM_CMD_DEV_IDX];
	iapbuf[IAP_SIZE - 2] = uart0Buff[COM_CMD_DEV_IDX + 1];
	iapbuf[IAP_SIZE - 1] = uart0Buff[COM_CMD_DEV_IDX + 2];
	
	return write_flash();
}

/******************************************************************************
//启动应用程序
//特别注意：0x3000与0x3004依use_sertor_list[0]定！
******************************************************************************/
__asm void ExceuteApplication(void)
{
		/* Load main stack pointer with application stack pointer initial value,
		   stored at first location of application area */
		ldr r0, =0x3000
		ldr r0, [r0]
		mov sp, r0

		/* Load program counter with application reset vector address, located at
		   second word of application area. */
		ldr r0, =0x3004
		ldr r0, [r0]
        BX  r0
}

/******************************************************************************
//跳转到app区域运行
******************************************************************************/
void iap_jump_app_s(void)
{
    if (((*(vu32*)use_sertor_list[0]) & 0x10000000) == 0x10000000) //检查栈顶地址是否合法.0x10000000是sram的起始地址,也是程序的栈顶地址
    {
        SCB->VTOR  = use_sertor_list[0];
        ExceuteApplication();
    }
}
