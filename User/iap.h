#ifndef __IAP_H
#define __IAP_H
#include "myinclude.h"

//lpc17xx扇区起始地址定义
//IAP程序占用了三个扇区12K的空间,剩下的空间全部是用户可编程空间
//LPC1751       - 32 KB
//LPC1752       - 64 KB
//LPC1754/64    - 128 KB
//LPC1756/65/66 - 256 KB
//LPC1758/68    - 512 KB
#define SECTOR_0_START      0x00000000 //4K
#define SECTOR_1_START      0x00001000 //4K
#define SECTOR_2_START      0x00002000 //4K
#define SECTOR_3_START      0x00003000 //4K
#define SECTOR_4_START      0x00004000 //4K
#define SECTOR_5_START      0x00005000 //4K
#define SECTOR_6_START      0x00006000 //4K
#define SECTOR_7_START      0x00007000 //4K，32K
#define SECTOR_8_START      0x00008000 //4K
#define SECTOR_9_START      0x00009000 //4K
#define SECTOR_10_START     0x0000A000 //4K
#define SECTOR_11_START     0x0000B000 //4K
#define SECTOR_12_START     0x0000C000 //4K
#define SECTOR_13_START     0x0000D000 //4K
#define SECTOR_14_START     0x0000E000 //4K
#define SECTOR_15_START     0x0000F000 //4K，64K
#define SECTOR_16_START     0x00010000 //32K
#define SECTOR_17_START     0x00018000 //32K，128K
#define SECTOR_18_START     0x00020000 //32K
#define SECTOR_19_START     0x00028000 //32K
#define SECTOR_20_START     0x00030000 //32K
#define SECTOR_21_START     0x00038000 //32K，256K
#define SECTOR_22_START     0x00040000 //32K
#define SECTOR_23_START     0x00048000 //32K
#define SECTOR_24_START     0x00050000 //32K
#define SECTOR_25_START     0x00058000 //32K
#define SECTOR_26_START     0x00060000 //32K
#define SECTOR_27_START     0x00068000 //32K
#define SECTOR_28_START     0x00070000 //32K
#define SECTOR_29_START     0x00078000 //32K，512K

//lpc1788扇区结束地址定义
#define SECTOR_0_END        0x00000FFF //4K
#define SECTOR_1_END        0x00001FFF //4K
#define SECTOR_2_END        0x00002FFF //4K
#define SECTOR_3_END        0x00003FFF //4K
#define SECTOR_4_END        0x00004FFF //4K
#define SECTOR_5_END        0x00005FFF //4K
#define SECTOR_6_END        0x00006FFF //4K
#define SECTOR_7_END        0x00007FFF //4K，32K
#define SECTOR_8_END        0x00008FFF //4K
#define SECTOR_9_END        0x00009FFF //4K
#define SECTOR_10_END       0x0000AFFF //4K
#define SECTOR_11_END       0x0000BFFF //4K
#define SECTOR_12_END       0x0000CFFF //4K
#define SECTOR_13_END       0x0000DFFF //4K
#define SECTOR_14_END       0x0000EFFF //4K
#define SECTOR_15_END       0x0000FFFF //4K，64K
#define SECTOR_16_END       0x00017FFF //32K
#define SECTOR_17_END       0x0001FFFF //32K，128K
#define SECTOR_18_END       0x00027FFF //32K
#define SECTOR_19_END       0x0002FFFF //32K
#define SECTOR_20_END       0x00037FFF //32K
#define SECTOR_21_END       0x0003FFFF //32K，256K
#define SECTOR_22_END       0x00047FFF //32K
#define SECTOR_23_END       0x0004FFFF //32K
#define SECTOR_24_END       0x00057FFF //32K
#define SECTOR_25_END       0x0005FFFF //32K
#define SECTOR_26_END       0x00067FFF //32K
#define SECTOR_27_END       0x0006FFFF //32K
#define SECTOR_28_END       0x00077FFF //32K
#define SECTOR_29_END       0x0007FFFF //32K，512K

#define IAP_DEFAULT_VALUE	0xFF
#define APP_CONFIG_ADDR 	SECTOR_2_END //配置地址，依use_sertor_list[0]定！

/******************************************************************************
//函数声明
******************************************************************************/
uint32_t iap_erase(void);
uint32_t iap_data(uint8_t *buf, uint8_t cnt);
uint32_t end_iap(void);
void iap_jump_app_s(void);

#endif
