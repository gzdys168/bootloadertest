#include "myinclude.h"

/******************************************************************************
//初始化系统
******************************************************************************/
void Init_Sysytem(void)
{
#ifdef WITH_RTT_FUNC
	SEGGER_RTT_Init();
	SEGGER_RTT_ConfigUpBuffer(0, NULL, NULL, 0, SEGGER_RTT_MODE_BLOCK_IF_FIFO_FULL);
#endif

	//初始化系统节拍
	Init_Systick();

	//串口0初始化
	InitUART0(115200);	

	//看门狗，正式使用时要放开！
	WDT_ClrTimeOutFlag();
	WDT_Init(WDT_CLKSRC_IRC, WDT_MODE_RESET);
	WDT_Start(3000000); //3s
}

/******************************************************************************
//主函数
******************************************************************************/
int main(void)
{
#ifdef WITH_RTT_FUNC
	uint32_t partid; //68: 26013F37; 65: 26013733
#endif
	
	Init_Sysytem();
	
#ifdef WITH_RTT_FUNC
    SEGGER_RTT_WriteString(0, "Bootloader started!\r\n"); //Debug
	
	if (ReadPartID(&partid) == CMD_SUCCESS)
	{
		SEGGER_RTT_printf(0, "part id: %x \r\n", partid);
	}
#endif
	
	while(1)
	{
		WDT_Feed();
		
		if (uart0cycle > MY_SYSTICK_50MS_CNT)
		{
			InitUart0Buff();
		}
	}
}

#ifdef DEBUG
/*******************************************************************************
* @brief		Reports the name of the source file and the source line number
* 				where the CHECK_PARAM error has occurred.
* @param[in]	file Pointer to the source file name
* @param[in]    line assert_param error line source number
* @return		None
*******************************************************************************/
void check_failed(uint8_t *file, uint32_t line)
{
	/* User can add his own implementation to report the file name and line number,
	 ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while(1);
}
#endif
