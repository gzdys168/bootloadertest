#ifndef _MYINCLUDE_H
#define _MYINCLUDE_H

/*******************************************************************************
//条件编译
*******************************************************************************/
//#define WITH_RTT_FUNC 1
#define WITH_AUTO_BAUDRATE_FUNC 1 //自动波特率（AT）

/******************************************************************************
//头文件
******************************************************************************/
#include <LPC17xx.h>

#include <stdio.h>
#include "stdlib.h"
#include "string.h"
#include "math.h"

#include "lpc17xx_systick.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_spi.h"
#include "lpc17xx_wdt.h"
#include "lpc17xx_uart.h"
#include "lpc17xx_iap.h"

#ifdef WITH_RTT_FUNC
	#include "SEGGER_RTT.h"
#endif

#include "bitband.h"

#include "myconst.h"

#include "systick.h"
#include "mystruct.h"

#include "comm.h"

#include "uart0.h"
#include "uart0cmd.h"

#endif
