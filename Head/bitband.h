//位带操作,实现51类似的GPIO控制功能
//具体实现思想,参考<<CM3权威指南>>第五章(87页~92页).
//IO口操作宏定义

#define BITBAND(addr, bitnum)	((addr & 0xF0000000) + 0x2000000 + ((addr & 0xFFFFF) << 5) + (bitnum << 2))
#define MEM_ADDR(addr)  		*((volatile unsigned long  *)(addr))
#define BIT_ADDR(addr, bitnum)	MEM_ADDR(BITBAND(addr, bitnum))

//IO口地址映射--------------------------------------------------

//输出寄存器

#define GPIO0_ODR_Addr    (LPC_GPIO0_BASE+0x18) //0x2009C018

#define GPIO1_ODR_Addr    (LPC_GPIO1_BASE+0x18) //0x2009C038

#define GPIO2_ODR_Addr    (LPC_GPIO2_BASE+0x18) //0x2009C058

#define GPIO3_ODR_Addr    (LPC_GPIO3_BASE+0x18) //0x2009C078

#define GPIO4_ODR_Addr    (LPC_GPIO4_BASE+0x18) //0x2009C098

//输入寄存器

#define GPIO0_IDR_Addr    (LPC_GPIO0_BASE+0x14) //0x2009C014

#define GPIO1_IDR_Addr    (LPC_GPIO1_BASE+0x14) //0x2009C034

#define GPIO2_IDR_Addr    (LPC_GPIO2_BASE+0x14) //0x2009C054

#define GPIO3_IDR_Addr    (LPC_GPIO3_BASE+0x14) //0x2009C074

#define GPIO4_IDR_Addr    (LPC_GPIO4_BASE+0x14) //0x2009C094

//方向寄存器

#define GPIO0_DIR_Addr    (LPC_GPIO0_BASE+0x00) //0x2009C000

#define GPIO1_DIR_Addr    (LPC_GPIO1_BASE+0x00) //0x2009C020

#define GPIO2_DIR_Addr    (LPC_GPIO2_BASE+0x00) //0x2009C040

#define GPIO3_DIR_Addr    (LPC_GPIO3_BASE+0x00) //0x2009C060

#define GPIO4_DIR_Addr    (LPC_GPIO4_BASE+0x00) //0x2009C080

//清零寄存器

#define GPIO0_CLS_Addr    (LPC_GPIO0_BASE+0x1C) //0x2009C01C

#define GPIO1_CLS_Addr    (LPC_GPIO1_BASE+0x1C) //0x2009C03C

#define GPIO2_CLS_Addr    (LPC_GPIO2_BASE+0x1C) //0x2009C05C

#define GPIO3_CLS_Addr    (LPC_GPIO3_BASE+0x1C) //0x2009C07C

#define GPIO4_CLS_Addr    (LPC_GPIO4_BASE+0x1C) //0x2009C09C

//IO口操作,只对单一的IO口,确保n的值小于32!--------------------------------

#define P0high(n)  BIT_ADDR(GPIO0_ODR_Addr,n)  //输出  0输出不变 1输出为1

#define P0low(n)   BIT_ADDR(GPIO0_CLS_Addr,n)  //清除  0输出不变 1输出0

#define P0in(n)    BIT_ADDR(GPIO0_IDR_Addr,n)  //输入

#define P0dir(n)   BIT_ADDR(GPIO0_DIR_Addr,n)  //方向 0输入1输出


#define P1high(n)  BIT_ADDR(GPIO1_ODR_Addr,n)  //输出  0输出不变 1输出为1

#define P1low(n)   BIT_ADDR(GPIO1_CLS_Addr,n)  //清除  0输出不变 1输出0

#define P1in(n)    BIT_ADDR(GPIO1_IDR_Addr,n)  //输入

#define P1dir(n)   BIT_ADDR(GPIO1_DIR_Addr,n)  //方向 0输入1输出


#define P2high(n)  BIT_ADDR(GPIO2_ODR_Addr,n)  //输出  0输出不变 1输出为1

#define P2low(n)   BIT_ADDR(GPIO2_CLS_Addr,n)  //清除  0输出不变 1输出0

#define P2in(n)    BIT_ADDR(GPIO2_IDR_Addr,n)  //输入

#define P2dir(n)   BIT_ADDR(GPIO2_DIR_Addr,n)  //方向 0输入1输出


#define P3high(n)  BIT_ADDR(GPIO3_ODR_Addr,n)  //输出  0输出不变 1输出为1

#define P3low(n)   BIT_ADDR(GPIO3_CLS_Addr,n)  //清除  0输出不变 1输出0

#define P3in(n)    BIT_ADDR(GPIO3_IDR_Addr,n)  //输入

#define P3dir(n)   BIT_ADDR(GPIO3_DIR_Addr,n)  //方向 0输入1输出


#define P4high(n)  BIT_ADDR(GPIO4_ODR_Addr,n)  //输出  0输出不变 1输出为1

#define P4low(n)   BIT_ADDR(GPIO4_CLS_Addr,n)  //清除  0输出不变 1输出0

#define P4in(n)    BIT_ADDR(GPIO4_IDR_Addr,n)  //输入

#define P4dir(n)   BIT_ADDR(GPIO4_DIR_Addr,n)  //方向 0输入1输出

//IO操作宏----------------------------------------------------------------

//IO口方向定义
#define IO_DIR_IN			   0
#define IO_DIR_OUT			   1

#define IODirIn(port, n)       P##dir(n) = IO_DIR_IN   //IO口输入
#define IODirOut(port, n)      P##dir(n) = IO_DIR_OUT  //IO口输出

#define SetIOHigh(port, n)     P##port##high(n) = 1 //IO口输出高
#define SetIOLow(port, n)      P##port##low(n) = 1  //IO口输出低

#define GetIOStatus(port, n)   P##port##in(n)       //读IO口状态

#define IOIsHigh(port, n)      (P##port##in(n) == 1) //IO口处于高
#define IOIsLow(port, n)       (P##port##in(n) == 0) //IO口处于低
